Serial server for MUR project.
Use serial communication between IDE and Intel Edison using mraa.

## Available functions ##
* Send binary file
* RUN/STOP program
* Get vehicle status
* Change system/wifi name
* Change system/wifi password