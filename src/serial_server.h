#pragma once

#include <map>
#include <set>
#include <chrono>
#include <thread>
#include <memory>
#include <string>
#include <vector>
#include <functional>

#include "zmq.hpp"
#include "msg_types.h"
#include "binary_storage.h"
#include "serial_connection.h"

enum class ReturnCode : uint8_t
{
    SUCCESS = 0x00,
    INCORRECT_NAME = 0x10,
    CHANGE_NAME_FAILED = 0x20,
    CHANGE_PASSWORD_FAILED = 0x30,
    CMD_EXEC_FAILED = 0x40,
    INCORRECT_CMD = 0X41,
    BINARY_CRC_FAILED = 0x50,
    UNKNOWN_COMMAND = 0XFF
};

class SerialServer
{
public:
    static SerialServer& instance();

    SerialServer(SerialServer&&) = delete;
    SerialServer(SerialServer const &) = delete;

    SerialServer& operator=(SerialServer&&) = delete;
    SerialServer& operator=(SerialServer const &) = delete;

    void update();
    void sleep() const;

protected:
    SerialServer();
    ~SerialServer();

private:
    const uint32_t m_sleepTimeMs = 1;
    std::unique_ptr<SerialConnection> m_connection;

    std::set<char> m_validSpecialChars = { '-', '_', ':', '+', '-', '=', '*' };
    std::map<InputPackageType, std::function<void (const std::vector<uint8_t>&)> > m_callbacks;

    // class for collect binary
    BinaryStorage m_binaryStorage;

    // Struct with vehicle status
    StatusInfo m_statusInfo;

    // zeromq stuff
    std::unique_ptr<zmq::context_t> m_zmqContext;
    std::unique_ptr<zmq::socket_t> m_zmqIdeSocket; // Emulation for ide
    std::unique_ptr<zmq::socket_t> m_zmqStatusSocket; // Subscriber for status info
    const int m_zmqSocketTimeout = 2000; // 2 seconds

    // check if charecter is one of the special symbols
    bool isSpecial(const char c) const;
    // check if string only consists of digits and letters and special symbols
    bool isValidString(std::string& str, size_t minSize) const;

    void registerCallback(InputPackageType type, std::function<void (const std::vector<uint8_t>&)> f);

    // callbacks
    // work with command from IDE (run/kill app)
    void handleCommand(const std::vector<uint8_t>& data);
    // change name and password commands
    void changeName(const std::vector<uint8_t>& data);
    void changePassword(const std::vector<uint8_t>& data);
    // receive binary app
    void handleBinaryPreamble(const std::vector<uint8_t>& data);
    void handleBinaryChunk(const std::vector<uint8_t>& data);
    void handleBinaryEnd(const std::vector<uint8_t>& data);
    // save BT MAC  
    void handleBluetoothMAC(const std::vector<uint8_t>& data);
    // get status info request
    void handleStatusInfoReq(const std::vector<uint8_t>& data);

    // Answers for commands
    void sendResult(ReturnCode retCode);
    void sendStatusInfo();

    // zmq helper
    ReturnCode getIdeSocketReply();

    // parse helper
    template <typename T>
    void deserialize(T& dest, const uint8_t*& source)
    {
        memcpy(&dest, source, sizeof(T));
        source += sizeof(T);
    }
};