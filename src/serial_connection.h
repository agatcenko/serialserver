#pragma once

#include "mraa.hpp"

#include <vector>
#include <memory>

struct SerialSettings
{
    int bytesize;
    int stopbits;
    unsigned int baudrate;
    mraa::UartParity parity;
    bool xonxoff;
    bool rtscts;

    SerialSettings(int _bytesize = 8, int _stopbits = 1, unsigned int _baudrate = 115200,
                   mraa::UartParity _parity = mraa::UART_PARITY_NONE, bool _xonxoff = false, bool _rtscts = false) :
        bytesize(_bytesize), stopbits(_stopbits), baudrate(_baudrate), parity(_parity), xonxoff(_xonxoff), rtscts(_rtscts) {}
};

enum class InputPackageType : uint8_t
{
    COMMAND_IN = 0x00,
    BINARY_PREAMBLE_IN = 0x10,
    BINARY_CHUNK_IN = 0x11,
    BINARY_END_IN = 0x12,
    CHANGE_NAME_IN = 0x20,
    CHANGE_PASSWORD_IN = 0x21,
    BT_MAC_IN = 0x30,
    STATUS_INFO_IN = 0x40,
    ERROR_IN = 0xFF
};

enum class OutputPackageType : uint8_t
{
    STATUS_INFO_OUT = 0x70,
    RESULT_OUT = 0x80
};

struct InputCommand
{
    InputPackageType id; // package id
    std::vector<uint8_t> data;
};

class SerialConnection
{
public:
    SerialConnection(std::string port, const SerialSettings& settings = SerialSettings());
    ~SerialConnection();

    int read();
    int sendPackage(OutputPackageType type, const std::vector<uint8_t> data);
    
    InputCommand parseInput();
        
private:
    std::vector<uint8_t> m_input;
    std::unique_ptr<mraa::Uart> m_dev;

    int configureConnection(const SerialSettings& settings);
};
