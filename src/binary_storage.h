#pragma once

#include <vector>

class BinaryStorage
{
public:
    void storeChunk(uint32_t number, std::vector<uint8_t> chunk);
    void init(uint16_t _chunkSize, uint16_t _crc, uint64_t _binSize,
              uint8_t nameSize, std::vector<uint8_t> name);

    bool verifyBinary() const; // compare crc
    std::vector<uint8_t> getBinary() const;

    uint8_t getBinaryNameSize() const;
    std::vector<uint8_t> getBinaryName() const;

private:
    uint16_t m_chunkSize;
    uint16_t m_binaryCRC;
    uint64_t m_binarySize;

    uint8_t m_nameSize;
    std::vector<uint8_t> m_name;
    std::vector<uint8_t> m_binary;
};