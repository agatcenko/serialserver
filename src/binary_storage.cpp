#include "hash_utils.h"
#include "binary_storage.h"

#include <algorithm>

using namespace std;

void BinaryStorage::init(uint16_t _chunkSize, uint16_t _crc, uint64_t _binSize,
                         uint8_t nameSize, vector<uint8_t> name)
{
    m_chunkSize = _chunkSize;
    m_binaryCRC = _crc;
    m_binarySize = _binSize;
    m_nameSize = nameSize;
    m_name = name;

    m_binary.resize(m_binarySize);
}

void BinaryStorage::storeChunk(uint32_t number, vector<uint8_t> chunk)
{   
    auto currentIt = number * m_chunkSize;
    if ((uint64_t)(number + 1) * m_chunkSize > m_binarySize) {
        copy(chunk.begin(), chunk.begin() + m_binarySize - currentIt,
             m_binary.data() + currentIt);
    }
    else {
        copy(chunk.begin(), chunk.end(), m_binary.data() + currentIt);
    }
}

bool BinaryStorage::verifyBinary() const
{
    uint16_t calcCRC = calculateCRC(m_binary.data(), m_binary.size());
    if (calcCRC != m_binaryCRC) {
        return false;
    }

    return true;
}

vector<uint8_t> BinaryStorage::getBinary() const
{
    return m_binary;
}

uint8_t BinaryStorage::getBinaryNameSize() const
{
    return m_nameSize;
}

vector<uint8_t> BinaryStorage::getBinaryName() const
{
    return m_name;
}
