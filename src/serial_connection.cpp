#include "hash_utils.h"
#include "serial_connection.h"

#include <iostream>
#include <functional>
#include <algorithm>

using namespace std;
using namespace mraa;

SerialConnection::SerialConnection(string port, const SerialSettings& settings)
{
    m_dev = unique_ptr<Uart>(new Uart(port));

    if (configureConnection(settings) == -1) {
        cout << "Configure connection failed" << endl;
    }    
}

int SerialConnection::configureConnection(const SerialSettings& settings)
{
    if (m_dev->setBaudRate(settings.baudrate) != SUCCESS) {
        cout << "Error setting baudrate on UART" << endl;
        return -1;
    }

    if (m_dev->setMode(settings.bytesize, settings.parity, settings.stopbits) != SUCCESS) {
        cout << "Error setting mode on UART" << endl;
        return -1;
    }

    if (m_dev->setFlowcontrol(settings.xonxoff, settings.rtscts) != SUCCESS) {
        cout << "Error setting flow control UART" << endl;
        return -1;
    }

    return 0;
}

SerialConnection::~SerialConnection()
{

}

int SerialConnection::read()
{
    vector<uint8_t> input;
    vector<uint8_t> buffer (512, 0);
    
    while (m_dev->dataAvailable()) {
        int bytes = m_dev->read((char *)buffer.data(), buffer.size());
        if (bytes) {
            copy(buffer.begin(), buffer.begin() + bytes, back_inserter(input));
        }
    }

    m_input = move(input);

    return m_input.size();
}

InputCommand SerialConnection::parseInput()
{
    InputCommand cmd;
    cmd.id = InputPackageType::ERROR_IN; // Error cmd

    cout << "Reseived: ";
    for (const auto b : m_input) {
        cout << (int)b << " ";
    }
    cout << endl;    

    vector<uint8_t> inputBody;
    uint8_t firstByte = 0x7D, secondByte = 0x7F;
    for (size_t i = 0; i < m_input.size() - 1; ++i) {
        if (m_input[i] == firstByte && m_input[i + 1] == secondByte) {
            auto bodyStart = i + 2;

            copy(m_input.begin() + bodyStart, m_input.end(), back_inserter(inputBody));

            break;
        }
    }

    if (inputBody.size()) {
        cmd.id = static_cast<InputPackageType>(inputBody[0]);
        auto dataSize = inputBody[2] << 8;
        dataSize |= inputBody[1];
        vector<uint8_t> data(dataSize);
        auto dataStartIt = inputBody.begin() + 3; // 3 - data offset
        auto dataEndIt = dataStartIt + dataSize;
        copy(dataStartIt, dataEndIt, data.data());
        cmd.data = data;
    }

    return cmd;
}

int SerialConnection::sendPackage(OutputPackageType type, const vector<uint8_t> data)
{
    vector<uint8_t> package;
    auto packageSize = data.size() + 2 /*preamble*/ + 1 /*type*/ + 2 /* data size */ + 2 /*crc*/+ 1 /*terminator*/;
    package.resize(packageSize);
    package[0] = 0x7D;
    package[1] = 0x7F;
    package[2] = static_cast<uint8_t>(type);
    uint16_t dataSize = data.size();
    memcpy(package.data() + 3, &dataSize, sizeof(dataSize));
    copy(data.begin(), data.end(), package.data() + 5);
    uint16_t crc = calculateCRC(package.data(), packageSize - 3);
    package[packageSize - 3] = crc & 0xFF;
    package[packageSize - 2] = crc >> 8;
    package[packageSize - 1] = 0x7E;

    cout << "Send: ";
    for (const auto b : package) {
        cout << (int)b << " ";
    }
    cout << endl;

    if (m_dev->write((const char*)package.data(), packageSize) == -1) {
        return -1;
    }

    return 0;
}
