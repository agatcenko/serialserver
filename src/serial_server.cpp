#include "serial_server.h"

#include <cctype>
#include <iostream>
#include <iterator>
#include <stdlib.h>
#include <algorithm>

using namespace std;
using namespace zmq;
using namespace placeholders;

using SerialServerCallback = function<void (const vector<uint8_t>&)>; 

#define CALLBACK(functionMember) \
    bind(functionMember, this, _1)

SerialServer& SerialServer::instance()
{
    static SerialServer server;
    return server;
}

SerialServer::SerialServer()
{
    m_connection = unique_ptr<SerialConnection>(new SerialConnection("/dev/ttyMFD1"));
    
    // register callback for input packages   
    registerCallback(InputPackageType::COMMAND_IN, CALLBACK(&SerialServer::handleCommand));
    registerCallback(InputPackageType::CHANGE_NAME_IN, CALLBACK(&SerialServer::changeName));
    registerCallback(InputPackageType::CHANGE_PASSWORD_IN, CALLBACK(&SerialServer::changePassword));
    registerCallback(InputPackageType::BINARY_PREAMBLE_IN, CALLBACK(&SerialServer::handleBinaryPreamble));
    registerCallback(InputPackageType::BINARY_CHUNK_IN, CALLBACK(&SerialServer::handleBinaryChunk));
    registerCallback(InputPackageType::BINARY_END_IN, CALLBACK(&SerialServer::handleBinaryEnd));
    registerCallback(InputPackageType::BT_MAC_IN, CALLBACK(&SerialServer::handleBluetoothMAC));
    registerCallback(InputPackageType::STATUS_INFO_IN, CALLBACK(&SerialServer::handleStatusInfoReq));

    // initialize zmq
    m_zmqContext = unique_ptr<context_t>(new context_t(1));
    m_zmqIdeSocket = unique_ptr<socket_t>(new socket_t(*m_zmqContext, ZMQ_REQ));
    m_zmqStatusSocket = unique_ptr<socket_t>(new socket_t(*m_zmqContext, ZMQ_SUB));
    m_zmqIdeSocket->connect("tcp://localhost:7350");
    m_zmqStatusSocket->connect("tcp://localhost:2052");

    // set zmq options for sockets
    try {
        m_zmqIdeSocket->setsockopt(ZMQ_SNDTIMEO, &m_zmqSocketTimeout, sizeof(int));
        m_zmqIdeSocket->setsockopt(ZMQ_RCVTIMEO, &m_zmqSocketTimeout, sizeof(int));
        m_zmqStatusSocket->setsockopt(ZMQ_SUBSCRIBE, 0, 0);
    }
    catch (const exception &e) {
        cout << "Error while set options for sockets" << endl;
    }
}

SerialServer::~SerialServer()
{

}

void SerialServer::update()
{
    if (m_connection->read()) {
        InputCommand cmd = m_connection->parseInput();    
        if (m_callbacks.count(cmd.id)) {
            m_callbacks[cmd.id](cmd.data);
        }
        else {
            cout << "Receive unknown command. No registered callback" << endl;
            sendResult(ReturnCode::UNKNOWN_COMMAND);
        }
    }
}

void SerialServer::sleep() const
{
    this_thread::sleep_for(chrono::milliseconds(m_sleepTimeMs));
}

bool SerialServer::isSpecial(const char c) const
{
    if (m_validSpecialChars.count(c)) {
        return true;
    }

    return false;
}

bool SerialServer::isValidString(string& str, size_t minSize) const
{
    if (str.size() < minSize) {
        return false;
    }

    for (const auto ch : str) {
        if (!isalnum(ch) && // not an alphanumeric char 
            !isSpecial(ch)) // not a special character
        {
            return false;
        }
    }

    return true;
}


void SerialServer::registerCallback(InputPackageType type, SerialServerCallback f)
{
    m_callbacks[type] = f;
}

void SerialServer::handleCommand(const std::vector<uint8_t>& data)
{
    cout << "handleCommand" << endl;

    uint8_t cmd;
    memcpy(&cmd, data.data(), sizeof(uint8_t));

    if (cmd == 0) { // kill app
        cmd = 40;
    }
    else if (cmd == 1) { // run app
        cmd = 30;
    }
    else {
        sendResult(ReturnCode::INCORRECT_CMD);
        return;
    }

    message_t zmqCommandMsg(sizeof(uint8_t));
    memcpy(zmqCommandMsg.data(), &cmd, sizeof(uint8_t));
    m_zmqIdeSocket->send(zmqCommandMsg);

    ReturnCode code = getIdeSocketReply();
    sendResult(code);
}

void SerialServer::changeName(const vector<uint8_t>& data)
{
    cout << "changeName" << endl;
    string name(data.begin(), data.end());

    ReturnCode ret = ReturnCode::SUCCESS;

    size_t MIN_NAME_SIZE = 5; // add to config
    if (isValidString(name, MIN_NAME_SIZE)) {
        string cmd = "configure_edison --changeName " + name;
        if (system(cmd.c_str()) == 0) {
            cout << "Name was changed" << endl;
        }
        else {
            ret = ReturnCode::CHANGE_NAME_FAILED;
        }
    }
    else {
        cout << "Name is incorrect" << endl;
        ret = ReturnCode::INCORRECT_NAME;
    }

    sendResult(ret);
}

void SerialServer::changePassword(const vector<uint8_t>& data)
{
    cout << "changePassword" << endl;
    string password(data.begin(), data.end());

    ReturnCode ret = ReturnCode::SUCCESS;

    size_t MIN_PASSWORD_SIZE = 8; // add to config
    if (isValidString(password, MIN_PASSWORD_SIZE)) {
        string cmd = "configure_edison --changePassword " + password;
        if (system(cmd.c_str()) != 0) {
            ret = ReturnCode::CHANGE_PASSWORD_FAILED;
        }
    }
    else {
        cout << "Password is incorrect" << endl;
        ret = ReturnCode::INCORRECT_NAME;
    }

    sendResult(ret);
}

void SerialServer::handleBinaryPreamble(const std::vector<uint8_t>& data)
{
    cout << "handleBinaryPreamble" << endl;
    auto dataPointer = data.data();

    // start data deserialization 
    uint8_t nameSize;
    deserialize(nameSize, dataPointer);

    vector<uint8_t> binaryName (nameSize);
    copy(data.begin() + 1, data.begin() + 1 + nameSize, binaryName.begin());
    dataPointer += nameSize;

    uint16_t chunkSize;
    deserialize(chunkSize, dataPointer);

    uint64_t binarySize;
    deserialize(binarySize, dataPointer);

    uint16_t binaryCRC;
    deserialize(binaryCRC, dataPointer);
    // end data deserialization

    m_binaryStorage.init(chunkSize, binaryCRC, binarySize, nameSize, binaryName);

    sendResult(ReturnCode::SUCCESS);
}

void SerialServer::handleBinaryChunk(const std::vector<uint8_t>& data)
{
    cout << "handleBinaryChunk" << endl;
    auto dataPointer = data.data();
    uint32_t chunkNumber;
    deserialize(chunkNumber, dataPointer);

    vector<uint8_t> chunk(data.begin() + sizeof(uint32_t), data.end());
    m_binaryStorage.storeChunk(chunkNumber, chunk);

    sendResult(ReturnCode::SUCCESS);
}

void SerialServer::handleBinaryEnd(const std::vector<uint8_t>& data)
{
    cout << "handleBinaryEnd" << endl;
    if (!m_binaryStorage.verifyBinary()) {
        cout << "Received binary has incorrect crc" << endl;
        sendResult(ReturnCode::BINARY_CRC_FAILED);
        return;
    }

    // command to prepare execserver for receiving new binary file
    uint8_t cmd = 10;
    message_t acceptFileCmd (sizeof(uint8_t));
    memcpy(acceptFileCmd.data(), &cmd, sizeof(uint8_t));
    m_zmqIdeSocket->send(acceptFileCmd);

    if (getIdeSocketReply() != ReturnCode::SUCCESS) {
        cout << "No answer from execserver on accept file cmd" << endl;
        sendResult(ReturnCode::CMD_EXEC_FAILED);
        return;
    }

    // send file name to execserver
    uint8_t nameSize = m_binaryStorage.getBinaryNameSize();
    vector<uint8_t> name = m_binaryStorage.getBinaryName();

    message_t fileNameCmd (nameSize);
    memcpy(fileNameCmd.data(), name.data(), nameSize);
    m_zmqIdeSocket->send(fileNameCmd);

    if (getIdeSocketReply() != ReturnCode::SUCCESS) {
        cout << "No answer from execserver on get file name" << endl;
        sendResult(ReturnCode::CMD_EXEC_FAILED);
        return;
    }

    // send file to exec server
    vector<uint8_t> binary = m_binaryStorage.getBinary();
    message_t binaryMsg (binary.size());
    memcpy(binaryMsg.data(), binary.data(), binary.size());
    m_zmqIdeSocket->send(binaryMsg);

    if (getIdeSocketReply() != ReturnCode::SUCCESS) {
        cout << "No answer from execserver on get binary file" << endl;
        sendResult(ReturnCode::CMD_EXEC_FAILED);
        return;
    }

    sendResult(ReturnCode::SUCCESS);
}

void SerialServer::handleBluetoothMAC(const std::vector<uint8_t>& data)
{
    cout << "handleBluetoothMAC" << endl;
}

void SerialServer::handleStatusInfoReq(const std::vector<uint8_t>& data)
{
    cout << "handleStatusInfoReq" << endl;
    // Try to read status info from server
    message_t statusMsg;
    if (m_zmqStatusSocket->recv(&statusMsg, ZMQ_NOBLOCK)) {
        memcpy(&m_statusInfo, statusMsg.data(), statusMsg.size());
    }

    sendStatusInfo();
}

void SerialServer::sendResult(ReturnCode retCode)
{
    cout << "sendResult with code: " << static_cast<int>(retCode) << endl;
    vector<uint8_t> data (1, static_cast<uint8_t>(retCode));
    if (m_connection->sendPackage(OutputPackageType::RESULT_OUT, data) != 0) {
        cout << "Error while sendResult" << endl;
    }
}

void SerialServer::sendStatusInfo()
{
    cout << "sendStatusInfo" << endl;
    vector<uint8_t> data (sizeof(StatusInfo));
    memcpy(data.data(), &m_statusInfo, sizeof(StatusInfo));
    if (m_connection->sendPackage(OutputPackageType::STATUS_INFO_OUT, data) != 0) {
        cout << "Error while sendStatusInfo" << endl;
    }
}

ReturnCode SerialServer::getIdeSocketReply()
{
    message_t reply;
    m_zmqIdeSocket->recv(&reply);

    ReturnCode code = ReturnCode::SUCCESS;
    if (!reply.size()) {
        code = ReturnCode::CMD_EXEC_FAILED;
    }

    return code;
}