#pragma once

#include <cstdint>

uint16_t updateCRC(uint16_t acc, const uint8_t input);

uint16_t calculateCRC(const uint8_t *data, const std::size_t len);