#include "serial_server.h"

using namespace std;

int main()
{
    while (true) {
        SerialServer::instance().update();
        SerialServer::instance().sleep();
    }

    return 0;
}