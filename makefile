ifndef BIN
	BIN = ./bin
endif

CXX = g++
CXXFLAGS = -std=c++11 -Wall

INCLUDES = -I. 
LIBS = -lzmq -lmraa

ccps = $(wildcard src/*.cpp)
objs = $(patsubst %.cpp,%.o,$(ccps))
ddds = $(wildcard *.d)

all: serialserver

include $(ddds)

serialserver: $(objs)
	mkdir -p $(BIN)
	$(CXX) $(CXXFLAGS) $^ $(INCLUDES) $(LIBS) -o $(BIN)/serialserver

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -MD $< $(INCLUDES) -o $@

clean:
	rm -rf src/*.d
	rm -rf src/*.o
	rm $(BIN)/serialserver